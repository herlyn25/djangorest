# Welcome to My APP!
# Description!
My app is to work out invoice in a restaurant

# Steps for installation
`1. pipenv install --> It creates a new virtual enviroment`

`2. pipenv shell --> It actives the virtual enviroment`

`3. python manage.py migrate --> It is used to create tables in the database`

`4. python manage.py createsuperuser --> It is used to create an admin user, It is necesary for the DFR login and you can have access to the API.`

# Remenber Something very important
You must create registers in the tables **(client, waiter, table, saucer_or_drink)** for what you can create registers in the table **invoice**, after you can give click in the menu **List ** and you select the menuitem  **invoice**, there you could add products to the invoices clicking in the button **Add Product**, this link will bring you to the page where you will colocate drinks and saucers to the **invoice** 
