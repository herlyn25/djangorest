from rest_framework import serializers
from restaurant.models import *


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'name', 'lastname', 'observations']


class WaiterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Waiter
        fields = ['id', 'name', 'lastname', 'lastname2']


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = SaucerDrink
        fields = ['name', 'type', 'amount']


class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Table
        fields = ['name', 'dinners_number']

class DetailInvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = DetailInvoice
        fields = ['invoice', 'saucer_or_drink', 'quantity']

    def to_representation(self, instance):
        self.fields['saucer_or_drink']= ProductSerializer(read_only=True)
        return super().to_representation(instance)

class InvoiceSerializer(serializers.ModelSerializer):
    products = DetailInvoiceSerializer(many=True, read_only=True)
    class Meta:
        model = Invoice
        fields = ['id', 'date', 'client', 'waiter', 'table', 'products']

    def to_representation(self, instance):
        self.fields['client'] = ClientSerializer(read_only=True)
        self.fields['waiter'] = WaiterSerializer(read_only=True)
        self.fields['table'] = TableSerializer(read_only=True)
        return super().to_representation(instance)






