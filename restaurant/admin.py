from django.contrib import admin
from restaurant.models import *

# Register your models here.

@admin.register(Waiter)
class Waiter(admin.ModelAdmin):
    list_display = ['name', 'lastname', 'lastname2']


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'lastname', 'observations']


@admin.register(Table)
class TableAdmin(admin.ModelAdmin):
    list_display = ['name', 'dinners_number', 'location']


@admin.register(SaucerDrink)
class SaucerDrinkAdmin(admin.ModelAdmin):
    list_display = ['id','name', 'type', 'amount']


@admin.register(DetailInvoice)
class DetailInvoiceAdmin(admin.ModelAdmin):
    list_display = ['id','invoice', 'saucer_or_drink', 'quantity']


@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_display = ['id', 'date', 'client', 'waiter', 'table']
