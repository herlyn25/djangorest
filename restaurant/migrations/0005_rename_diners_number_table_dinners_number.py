# Generated by Django 4.0.4 on 2022-05-10 03:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restaurant', '0004_remove_detailinvoice_saucer_or_drink_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='table',
            old_name='diners_number',
            new_name='dinners_number',
        ),
    ]
