from django.db.models import Q
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import DeleteView
from restaurant.forms import *
from restaurant.serializer import *


# Create your views here.
def home(request):
    return render(request, "base.html")


def create_client(request):
    form = ClientForm()
    if request.method == 'POST':
        form = ClientForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')

    context = {'form': form, 'title': "Create Client", 'button': "Save Client"}
    return render(request, "restaurant/create_elements.html", context)


def create_waiter(request):
    form = WaiterForm()
    if request.method == 'POST':
        form = WaiterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/")

    context = {'form': form, 'title': "Create Waiter", 'button': "Save Waiter"}
    return render(request, "restaurant/create_elements.html", context)


def create_table(request):
    form = TableForm()
    if request.method == 'POST':
        form = TableForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/")

    context = {'form': form, 'title': "Create Table", 'button': "Save Table"}
    return render(request, "restaurant/create_elements.html", context)


def create_saucer_or_drink(request):
    form = SaucerDrinkForm()
    if request.method == 'POST':
        form = SaucerDrinkForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/")

    context = {'form': form, 'title': "Create Product", 'button': "Save Product"}
    return render(request, "restaurant/create_elements.html", context)


def create_invoice(request):
    form = InvoiceForm()
    if request.method == 'POST':
        form = InvoiceForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    context = {'form': form, 'title': "Create invoice", 'button': "Save Invoice"}

    return render(request, "restaurant/create_invoice.html", context)


def create_detail_invoice(request, pk):
    detail_invoice = DetailInvoice.objects.filter(invoice=pk)
    total = 0
    detail_invoice2 = DetailInvoice.objects.all().filter(invoice=pk).values('saucer_or_drink__amount', 'quantity')
    for item in detail_invoice2:
        total += item['saucer_or_drink__amount'] * item['quantity']
    form = DetailInvoiceForm()
    if request.method == "POST":
        data = {'invoice': request.POST['invoice'],
                'saucer_or_drink': request.POST['saucer_or_drink'],
                'quantity': request.POST['quantity']
                }
        form = DetailInvoiceForm(data)
        if form.is_valid():
            try:
                detail = DetailInvoice.objects.get(Q(invoice=data['invoice']) & Q(saucer_or_drink=data['saucer_or_drink']))
                if detail:
                    quantity = detail.quantity + int(data['quantity'])
                    DetailInvoice.objects.filter(id=detail.id).update(quantity=quantity)
            except Exception:
                DetailInvoice.objects.create(invoice_id=data['invoice'],
                                             saucer_or_drink_id=data['saucer_or_drink'],
                                             quantity=data['quantity'])
            detail_invoice2 = DetailInvoice.objects.all().filter(invoice=pk).values('saucer_or_drink__amount','quantity')
            total = 0
            for item in detail_invoice2:
                total += item['saucer_or_drink__amount'] * item['quantity']

    context = {'form': form, 'title': "Add products", 'button': "Save", 'entity': detail_invoice, 'pk': pk, 'total': total}
    return render(request, 'restaurant/create_detail_invoice.html', context)


def list_client(request):
    entity = Client.objects.all()
    context = {'entity': entity, 'title': "List of clients"}
    return render(request, "restaurant/list_clients.html", context)


def list_waiter(request):
    entity = Waiter.objects.all()
    context = {'entity': entity, 'title': "List of waiters"}
    return render(request, "restaurant/list_waiter.html", context)


def list_table(request):
    entity = Table.objects.all()
    context = {'entity': entity, 'title': "List of tables"}
    return render(request, "restaurant/list_tables.html", context)


def list_product(request):
    entity = SaucerDrink.objects.all()
    context = {'entity': entity, 'title': "List of products"}
    return render(request, "restaurant/list_product.html", context)


def list_invoice(request):
    entity = Invoice.objects.all()
    context = {'entity': entity, 'title': "List of invoices"}
    return render(request, "restaurant/list_invoices.html", context)


def detail_invoice(request, pk):
    invoice = Invoice.objects.get(id=pk)
    detail_invoice = DetailInvoice.objects.all().filter(invoice=pk)
    detail_invoice2 = DetailInvoice.objects.all().filter(invoice=pk).values('saucer_or_drink__amount', 'quantity')
    total = 0
    for item in detail_invoice2:
        total += item['saucer_or_drink__amount'] * item['quantity']

    context = {'entity': detail_invoice, 'title': "Detail Invoice",
               'invoice': invoice, 'total': total}

    return render(request, "restaurant/list_invoice_detail.html", context)


def update_client(request, pk):
    client = Client.objects.get(id=pk)
    form = ClientForm(instance=client)
    if request.method == "POST":
        form = ClientForm(request.POST, instance=client)
        if form.is_valid():
            form.save()
            return redirect("list_client")
    context = {'form': form, 'title': "Update client", 'button': "Update",
               'cancel': "Cancel", 'url': "list_client"}
    return render(request, "restaurant/update_element_form.html", context)


def update_waiter(request, pk):
    waiter = Waiter.objects.get(id=pk)
    form = WaiterForm(instance=waiter)
    if request.method == "POST":
        form = WaiterForm(request.POST, instance=waiter)
        if form.is_valid():
            form.save()
            return redirect("list_waiter")
    context = {'form': form, 'title': "Update waiter",
               'button': "Update", 'cancel': "Cancel",
               'url': 'list_waiter'}
    return render(request, "restaurant/update_element_form.html", context)


def update_product(request, pk):
    product = SaucerDrink.objects.get(id=pk)
    form = SaucerDrinkForm(instance=product)
    if request.method == "POST":
        form = SaucerDrinkForm(request.POST, instance=product)
        if form.is_valid():
            form.save()
            return redirect("list_client")
    context = {'form': form, 'title': "Update product",
               'button': "Update", 'cancel': "Cancel",
               'url': "list_product"}
    return render(request, "restaurant/update_element_form.html", context)


def update_table(request, pk):
    table = Table.objects.get(id=pk)
    form = TableForm(instance=table)
    if request.method == "POST":
        form = TableForm(request.POST, instance=table)
        if form.is_valid():
            form.save()
            return redirect("list_table")
    context = {'form': form, 'title': "Update table",
               'button': "Update", 'cancel': "Cancel",
               'url': "list_table"}
    return render(request, "restaurant/update_element_form.html", context)


class DeleteClientView(DeleteView):
    model = Client
    template_name = "restaurant/confirm_client_delete.html"
    success_url = reverse_lazy('list_client')


class DeleteWaiterView(DeleteView):
    model = Waiter
    template_name = "restaurant/confirm_client_delete.html"
    success_url = reverse_lazy('list_waiter')


class DeleteSaucerOrDrinkView(DeleteView):
    model = SaucerDrinkForm
    template_name = "restaurant/confirm_client_delete.html"
    success_url = reverse_lazy('list_product')


class DeleteTableView(DeleteView):
    model = Table
    template_name = "restaurant/confirm_table_delete.html"
    success_url = reverse_lazy('list_table')
