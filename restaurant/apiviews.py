from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet
from restaurant.serializer import *

class ClientViewSet(ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = [permissions.IsAuthenticated]


class WaiterViewSet(ModelViewSet):
    queryset = Waiter.objects.all()
    serializer_class = WaiterSerializer
    permission_classes = [permissions.IsAuthenticated]


class TableViewSet(ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer
    permission_classes = [permissions.IsAuthenticated]


class ProductViewSet(ModelViewSet):
    queryset = SaucerDrink.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticated]


class InvoiceViewSet(ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    permission_classes = [permissions.IsAuthenticated]


class DetailInvoiceViewSet(ModelViewSet):
    queryset = DetailInvoice.objects.all()
    serializer_class = DetailInvoiceSerializer
    permission_classes = [permissions.IsAuthenticated]