from django.db import models


# Create your models here.
class Client(models.Model):
    name = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    observations = models.TextField()

    def __str__(self):
        return self.name


class Waiter(models.Model):
    name = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    lastname2 = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class SaucerDrink(models.Model):
    CHOICES_TYPE = (('drink', 'Drink'), ('saucer', 'Saucer'))
    name = models.CharField(max_length=150)
    type = models.CharField(max_length=20, choices=CHOICES_TYPE)
    amount = models.FloatField()

    def __str__(self):
        return self.name


class Table(models.Model):
    CHOICE_LOCATION = (('planta baja', 'Planta Baja'), ('planta alta', 'Planta Alta'))
    dinners_number = models.IntegerField()
    name = models.CharField(max_length=30)
    location = models.CharField(max_length=20, choices=CHOICE_LOCATION)

    def __str__(self):
        return self.name


class Invoice(models.Model):
    date = models.DateField(auto_now_add=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    waiter = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    table = models.ForeignKey(Table, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)


class DetailInvoice(models.Model):
    date = models.DateField(auto_now_add=True)
    invoice = models.ForeignKey(Invoice, related_name="products", on_delete=models.CASCADE)
    saucer_or_drink = models.ForeignKey(SaucerDrink, on_delete=models.CASCADE)
    quantity = models.IntegerField()

    def __str__(self):
        return f'{self.saucer_or_drink.name}, {self.saucer_or_drink.type} ,{self.quantity}, {self.saucer_or_drink.amount}'
