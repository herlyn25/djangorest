from django import forms
from .models import *

class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        exclude = ['id']

class WaiterForm(forms.ModelForm):
    class Meta:
        model = Waiter
        exclude = ['id']

class TableForm(forms.ModelForm):
    class Meta:
        model = Table
        exclude = ['id']

class SaucerDrinkForm(forms.ModelForm):
    class Meta:
        model = SaucerDrink
        exclude = ['id']

class InvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoice
        exclude = ['id']
class DetailInvoiceForm(forms.ModelForm):
    class Meta:
        model = DetailInvoice
        fields = ['saucer_or_drink','quantity']