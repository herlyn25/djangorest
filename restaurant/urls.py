from django.urls import path
from rest_framework import routers
from restaurant.apiviews import *
from restaurant.views import *

router = routers.DefaultRouter()
router.register('clients', ClientViewSet)
router.register('waiters', WaiterViewSet)
router.register('products', ProductViewSet)
router.register('tables', TableViewSet)
router.register('invoices',InvoiceViewSet)
router.register('invoicedetails',DetailInvoiceViewSet)

urlpatterns = [
    path("", home, name="home"),
    path("createclient/", create_client, name='create_client'),
    path("createwaiter/", create_waiter, name='create_waiter'),
    path("createtable/", create_table, name='create_table'),
    path("createproduct/", create_saucer_or_drink, name='create_product'),
    path("createinvoice/", create_invoice, name='create_invoice'),
    path("createdetailinvoice/<int:pk>/", create_detail_invoice, name='create_detail_invoice'),
    path("listclient/", list_client, name='list_client'),
    path("listwaiter/", list_waiter, name='list_waiter'),
    path("listproduct/", list_product, name='list_product'),
    path("listtable/", list_table, name='list_table'),
    path("listinvoice/", list_invoice, name='list_invoice'),
    path("detailinvoice/<int:pk>/", detail_invoice, name='detail_invoice'),
    path("updateclient/<int:pk>/", update_client, name='update_client'),
    path("updatewaiter/<int:pk>/", update_waiter, name='update_waiter'),
    path("updateproduct/<int:pk>/", update_product, name='update_product'),
    path("updatetable/<int:pk>/", update_table, name='update_table'),
    path("delete_client/<int:pk>/", DeleteClientView.as_view(), name='delete_client'),
    path("delete_waiter/<int:pk>/", DeleteWaiterView.as_view(), name='delete_waiter'),
    path("delete_product/<int:pk>/", DeleteWaiterView.as_view(), name='delete_product'),
    path("delete_table/<int:pk>/", DeleteTableView.as_view(), name='delete_table'),
]
